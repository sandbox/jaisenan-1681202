Instant Search - Node, is a simple module for instant searching nodes 
published in the site. This is not having any relation with the 
Drupal default Search.

Install
-------

1) Copy the project folder to the modules folder in your installation.

2) Enable the module using Administer -> Site building -> Modules
   (/admin/build/modules).

3) Access the search page using /instant_search Url
